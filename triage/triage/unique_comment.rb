# frozen_string_literal: true
require 'active_support/inflector' # to use .demodulize

module Triage
  class UniqueComment
    NOTES_PER_PAGE = 100

    def initialize(class_name, event, unique_comment_identifier = nil, from: nil)
      @class_name = class_name
      @event = event
      @unique_comment_identifier = unique_comment_identifier
      @from = from
    end

    def no_previous_comment?
      resource_notes.none? do |note|
        matching_comment?(note)
      end
    end

    def previous_discussion
      @previous_discussion ||= resource_discussions.find do |discussion|
        discussion['notes'].find do |note|
          matching_comment?(note)
        end
      end
    end

    def previous_discussion_comment
      return unless previous_discussion

      @previous_discussion_comment ||= previous_discussion['notes'].find do |note|
        matching_comment?(note)
      end
    end

    def wrap(message)
      "#{hidden_comment}\n#{message}"
    end

    private

    attr_reader :class_name, :event, :unique_comment_identifier, :from

    def hidden_comment
      hidden_comment = unique_comment_identifier || class_name.demodulize
      "<!-- triage-serverless #{hidden_comment} -->"
    end

    def matching_comment?(note)
      # note is a Gitlab::ObjectifiedHash which is not a hash
      (from.nil? || from == note.to_h.dig('author', 'username')) &&
        note.body.include?(hidden_comment)
    end

    def resource_notes
      Triage.api_client.public_send("#{event.object_kind}_notes", project_id, resource_iid, per_page: NOTES_PER_PAGE).auto_paginate
    end

    def resource_discussions
      path = "/projects/#{project_id}/#{event.object_kind}s/#{resource_iid}/discussions"

      # merge_request_discussions do not support passing per_page
      Triage.api_client.get(path, query: { per_page: NOTES_PER_PAGE }).auto_paginate
    end

    def resource_iid
      event.iid
    end

    def project_id
      event.project_id
    end
  end
end
