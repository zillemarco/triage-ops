# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

require_relative '../../triage/rate_limit'
require_relative '../../../lib/team_member_select_helper'

module Triage
  class CommandMrHelp < CommunityProcessor
    include RateLimit
    include TeamMemberSelectHelper

    react_to 'merge_request.note'
    define_command name: 'help'

    def applicable?
      valid_command?
    end

    def process
      comment = <<~MARKDOWN.chomp
        Hey there #{coach}, could you please help @#{event.event_actor_username} out?
        /assign_reviewer #{coach}
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor supports the "@gitlab-bot help" triage operation command to assign a merge request coach as code reviewer.
      TEXT
    end

    private

    def coach
      @coach ||= select_random_merge_request_coach
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("help-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      event.by_team_member? ? 100 : 1
    end

    def rate_limit_period
      3600 # 1 hour
    end
  end
end
