# Implementing a new processor

The steps to implement a new processors are as follows:

1. Create a new subclass of `Processor` in `triage/processor/`.
1. If the processor is specific to a group, add the processor in a group-specific directory, e.g. `triage/processor/<group-name>/<file-name>_processor.rb`
1. General processor should live in `triage/processor/` or their relevant subdirectory.
1. Implement the processor itself by following the guidelines below.
1. Add the new processor to the `DEFAULT_PROCESSORS` array in [`triage/triage/handler.rb`](triage/triage/handler.rb).

Here is an example for a processor that posts a simple thank you message
when a merge request is created:

```ruby
module Triage
  class ThankYou < Processor
    react_to 'merge_request.open'

    def applicable?
      event.from_gitlab_org?
    end

    def process
      add_comment('Thank you for the contribution!', append_source_link: false)
    end
  end
end
```

For convenience, you can run the `bin/generate_processor` script which will take care of these steps for you.

Simply pass it the processor name (undescored or camel-cased) and optional events to react to. For instance:

```
bin/generate_processor --name default_label_upon_closing --react-to issue.close,merge_request.close,merge_request.merge
```

By default, the processor will react to `issue.*` and `merge_request.*`.

## Define what event to react to with `react_to`

The `react_to` DSL method allows you to declare a list of events for the processor to react upon.

The format is `RESOURCE.ACTION` where `RESOURCE` can be:

* `issue`
* `merge_request`
* `*` (means all of above)

And where `ACTION` can be different for different resources.

### Current list of actions for `issue`

* `open`
* `update`
* `note`
* `close`
* `reopen`
* `*` (means all of above)

### Current list of actions for `merge_request`

* `open`
* `update`
* `note`
* `close`
* `reopen`
* `approval`
* `unapproval`
* `approved`
* `unapproved`
* `merge`
* `*` (means all of above)

## Processor methods to implement

### The `#applicable?` method

Use the `#applicable` method to filter the events the processor should react to.

When using this method, call public `Triage::Event` methods on the `event` attribute.

### The `#process` method

The `#process` method is the only one that's required to implement.

Usually, processors post a note in the resource being processed with the `#add_comment(body, append_source_link: false)` or `#add_discussion(body, append_source_link: false)` methods.

## Processor methods available

### The `#add_comment(body, append_source_link: false)`

Use this method to post a simple note in the resource.

### The `#add_discussion(body, append_source_link: false)`

Use this method to create a new discussion in the resource.

## Define a reactive command to react to with `define_command`

With the `define_command` DSL method, you can declare a reactive command for the processor to react upon and take three keyword arguments:

- Use `name:` to declare the command to react to. For instance, `define_command name: 'shrug'` reacts to `@gitlab-bot shrug`.
- (Optional) Use `aliases:` to declare command aliases (for back-compatibility or shorter commands).
- (Optional) Use `args_regex:` to declare how to detect arguments passed to the command.
  For instance, `define_command name: 'shrug', args_regex: /\w+/` reacts to `@gitlab-bot shrug foo bar`.

Note that:

- The `define_command` method gives access to a `ReactiveCommand::Command` object through the `#command` method.
- You'll need to add a call to `command.valid?(event)` to the processor's `#applicable?` method.
- Arguments are detected through `args_regex` with the `command.args(event)` method, which returns an array of strings. For instance, `%w[foo bar]`.

## Roll-out a processor gradually with `percentage_rollout`

The `percentage_rollout` DSL method allows a processor to be rolled-out gradually based on a given threshold value, and takes two keyword arguments:

- `threshold:`, to declare the command to react to.
- `on:`, to declare a lambda that returns a string event identifier used to seed a random integer and compare it against the threshold.

For instance, `percentage_rollout threshold: 50` makes the processor process 50% of the events.

Note that:

- The `percentage_rollout` method gives access to a `PercentageRollout::Rollout` object through the `#rollout` method.
- You'll need to add a call to `command.rolled_out?(event)` to the processor's `#applicable?` method.
