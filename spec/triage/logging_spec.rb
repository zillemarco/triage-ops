# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/logging'

describe Triage::Logging do
  describe '.sucker_punch_logger' do
    it 'returns a Ougai::ChildLogger logger with field name=sucker_punch' do
      logger = described_class.sucker_punch_logger

      expect(logger).to be_kind_of(Ougai::ChildLogger)
      expect(logger.with_fields[:name]).to eq('sucker_punch')
    end

    it 'returns a persistent logger' do
      logger0 = described_class.sucker_punch_logger
      logger1 = described_class.sucker_punch_logger

      expect(logger0.object_id).to eq(logger1.object_id)
    end
  end

  describe '.rack_logger' do
    it 'returns a Ougai::ChildLogger logger with field name=rack' do
      logger = described_class.rack_logger

      expect(logger).to be_kind_of(Ougai::ChildLogger)
      expect(logger.with_fields[:name]).to eq('rack')
    end

    it 'returns a persistent logger' do
      logger0 = described_class.rack_logger
      logger1 = described_class.rack_logger

      expect(logger0.object_id).to eq(logger1.object_id)
    end
  end
end
