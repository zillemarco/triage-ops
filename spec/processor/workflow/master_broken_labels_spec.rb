# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/workflow/master_broken_labels'

RSpec.describe Triage::Workflow::MasterBrokenLabels do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        from_gitlab_org?: true
      }
    end

    let(:label_names) { [Labels::MASTER_BROKEN_LABEL] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when no labels are set' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when "pipeline:expedite-master-fixing" is set' do
      let(:label_names) { [Labels::PIPELINE_EXPEDITE_MASTER_FIXING_LABEL] }

      include_examples 'event is applicable'
    end

    context 'when "master:broken" is set and "pipeline:expedite-master-fixing" is set' do
      let(:label_names) { [Labels::MASTER_BROKEN_LABEL, Labels::PIPELINE_EXPEDITE_MASTER_FIXING_LABEL] }

      include_examples 'event is not applicable'
    end

    context 'when "master:foss-broken" is set and "pipeline:expedite-master-fixing" is set' do
      let(:label_names) { [Labels::MASTER_FOSS_BROKEN_LABEL, Labels::PIPELINE_EXPEDITE_MASTER_FIXING_LABEL] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples '"pipeline:expedite-master-fixing" label is added' do
      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          /label ~"#{Labels::PIPELINE_EXPEDITE_MASTER_FIXING_LABEL}"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when "master:broken" is set and "pipeline:expedite-master-fixing" is not set' do
      let(:label_names) { [Labels::MASTER_BROKEN_LABEL] }

      it_behaves_like '"pipeline:expedite-master-fixing" label is added'
    end

    context 'when "master:foss-broken" is set and "pipeline:expedite-master-fixing" is not set' do
      let(:label_names) { [Labels::MASTER_FOSS_BROKEN_LABEL] }

      it_behaves_like '"pipeline:expedite-master-fixing" label is added'
    end

    context 'when "master:broken" is not set and "pipeline:expedite-master-fixing" is set' do
      let(:label_names) { [Labels::PIPELINE_EXPEDITE_MASTER_FIXING_LABEL] }

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          Setting ~"#{Labels::PIPELINE_EXPEDITE_MASTER_FIXING_LABEL}" without ~"#{Labels::MASTER_BROKEN_LABEL}" or ~"#{Labels::MASTER_FOSS_BROKEN_LABEL}" is forbidden!
          /unlabel ~"#{Labels::PIPELINE_EXPEDITE_MASTER_FIXING_LABEL}"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
