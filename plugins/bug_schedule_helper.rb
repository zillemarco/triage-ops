# frozen_string_literal: true

require_relative '../lib/bug_schedule_helper'

Gitlab::Triage::Resource::Context.include BugScheduleHelper
